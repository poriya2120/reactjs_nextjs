import Head from 'next/head'
import Layout from '../components/Layout/Layout'
import styles from '../styles/Home.module.css'
import SearchInput from '../components/Layout/SerchInput/SerchInput';
export default function Home({conutries}) {
  console.log(conutries);

  return 
  <Layout>
    <div className={styles.counts} >{conutries.length} countries</div>
  <SearchInput/>
  </Layout>;
}

export const getStaticPros=async()=>{
  const res=await fetch("https://restcountries.eu/rest/v2/all");
  const conutries=await res.json();
  return{
    props:{
      conutries,
    },
  };
};
