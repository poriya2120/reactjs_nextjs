import style from './SearchInput.module.css'
import SearchRounded from './SearchRounded'


const  SearchInput=({...rest})=>{
    return <div className={style.waraper}>
       <SearchRounded color="ingerit"/>
        <input className={style.input} {...rest}/>
    </div>
}
export default SearchInput